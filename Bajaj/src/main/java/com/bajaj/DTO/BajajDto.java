package com.bajaj.DTO;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class BajajDto {

	public String  is_success;
	public String user_id;
	public String email;
	public String roll_number;
	public List<Integer> numbers;
	public List<String> alphabets;
	
	public BajajDto() {
		// TODO Auto-generated constructor stub
	}

	public BajajDto(String is_success, String user_id, String email, String roll_number, List<Integer> numbers,
			List<String> alphabets) {
		super();
		this.is_success = is_success;
		this.user_id = user_id;
		this.email = email;
		this.roll_number = roll_number;
		this.numbers = numbers;
		this.alphabets = alphabets;
	}

	public String getStatus() {
		return is_success;
	}

	public void setStatus(String is_success) {
		this.is_success = is_success;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRoll_number() {
		return roll_number;
	}

	public void setRoll_number(String roll_number) {
		this.roll_number = roll_number;
	}

	public List<Integer> getNumbers() {
		return numbers;
	}

	public void setNumbers(List<Integer> numbers) {
		this.numbers = numbers;
	}

	public List<String> getAlphabets() {
		return alphabets;
	}

	public void setAlphabets(List<String> alphabets) {
		this.alphabets = alphabets;
	}

	@Override
	public String toString() {
		return "BajajDto [is_success=" + is_success + ", user_id=" + user_id + ", email=" + email + ", roll_number="
				+ roll_number + ", numbers=" + numbers + ", alphabets=" + alphabets + "]";
	}
	
	
}
